# Plataforma Administradora de Consejo (PACO)

## Tooling
    - git
    - node
    - yarn (optional)
    - vue2
    - Firebase

### Editor (optional)

#### vsCode

    - vetur

## Installation

```bash
npm i
```
or

```bash
yarn
```

## Run
```bash
yarn run serve
```
```bash
npm run serve
```
