export default {
  documents: null,
  documentNameToCreate: '',
  documentDeletionPending: [],
  documentCreationPending: false,
  documentCreationMessage: {},
}
