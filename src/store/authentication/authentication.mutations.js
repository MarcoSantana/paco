export default {
  setUser: (state, value) => (state.user = value),
  setUserClaims: (state, value) => (state.userClaims = value),
}
